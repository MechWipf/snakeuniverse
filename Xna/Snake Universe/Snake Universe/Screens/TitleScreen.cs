﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace SnakeUniverse
{
    public class TitleScreen : GameScreen
    {
        KeyboardState keyState;
        SpriteFont Font;
        SpriteFont ActiveFont;
        List<string>Menu = new List<string>();
        int Active = 0;
        public float Elapsed;
        bool Initialize = false;

        public override void LoadContent()
        {
            base.LoadContent();
            ActiveFont = ScreenManager.Game.Content.Load<SpriteFont>("Fonts/ActiveBigFont");
            Font = ScreenManager.Game.Content.Load<SpriteFont>("Fonts/BigFont");
            if (Initialize == false)
            {
                Menu.Add("Survival");
                Menu.Add("TimeAttack");
                Menu.Add("Splashscreen");
                Menu.Add("Ende");
            }
            Initialize = true;

        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (IsActive == true)
            {
                Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                keyState = Keyboard.GetState();
                #region Menüsteuerung
                if (keyState.IsKeyDown(Keys.Down) && Elapsed > 0.3)
                {
                    Elapsed = 0;
                    if (Active == Menu.Count() - 1)
                        Active = 0;
                    else
                        Active = Active + 1;
                }
                if (keyState.IsKeyDown(Keys.Up) && Elapsed > 0.3)
                {
                    Elapsed = 0;
                    if (Active == 0)
                        Active = Menu.Count() - 1;
                    else
                        Active = Active - 1;
                }
                #endregion
                #region Menülogik
                if (keyState.IsKeyDown(Keys.Enter) && Elapsed > 1)
                {
                    switch (Active)
                    {
                        case 0:
                            //ScreenManager.AddScreen(new GamePlayScreen());
                            ScreenManager.AddScreen(new Survival());
                            break;
                        case 1:
                            ScreenManager.AddScreen(new TimeAttack());
                            break;
                        case 2:
                            ScreenManager.AddScreen(new SplashScreen());
                            break;
                        case 3:
                            ScreenManager.Game.Exit();
                            break;
                        default:
                            break;
                    }
                }
                #endregion

                base.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (IsActive == true)
            {
                ScreenManager.SpriteBatch.Begin();
                #region Menü
                for (int i = 0; i < Menu.Count(); i++)
                {
                    if (i == Active)
                        ScreenManager.SpriteBatch.DrawString(ActiveFont, "" + Menu[i], new Vector2(200, 150 + i * 100), Color.Red);
                    else
                        ScreenManager.SpriteBatch.DrawString(Font, "" + Menu[i], new Vector2(200, 150 + i * 100), Color.Black);
                }
                #endregion
                ScreenManager.SpriteBatch.End();
                
            }
        }
    }
}
