﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace SnakeUniverse
{
    public class PauseScreen : GameScreen
    {

        #region Initializations
        Texture2D Splash;
        SpriteFont Font;
        public float Elapsed;
        bool Initialize = false;
        List<string> Menu = new List<string>();
        int Active = 0;
        Texture2D Background;

        #endregion
        
        #region LoadContent and UnloadContent
        public override void LoadContent()
        {
            if (Initialize == false)
            {
                Splash = ScreenManager.Game.Content.Load<Texture2D>("Textures/Backgrounds/Splash");
                Font = ScreenManager.Game.Content.Load<SpriteFont>("Fonts/BigFont");
                Background= ScreenManager.Game.Content.Load<Texture2D>("Textures/Backgrounds/Pause");
                Menu.Add("Weiterspielen");
                Menu.Add("Optionen");
                Menu.Add("Hauptmenu");

            }
            Initialize = true;

            base.LoadContent();
        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }
        #endregion

        #region Update
        public override void Update(GameTime gameTime)
        {
            if (IsActive == true)
            {
                
                KeyboardState keyState = new KeyboardState();
                keyState = Keyboard.GetState();
                Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                ArrayList GamePads = new ArrayList();
                GamePads.Add(GamePad.GetState(PlayerIndex.One));
                GamePads.Add(GamePad.GetState(PlayerIndex.Two));
                GamePads.Add(GamePad.GetState(PlayerIndex.Three));
                GamePads.Add(GamePad.GetState(PlayerIndex.Four));

                #region Menüsteuerung
                if (keyState.IsKeyDown(Keys.Down) && Elapsed > 0.3)
                {
                    Elapsed = 0;
                    if (Active == Menu.Count() - 1)
                        Active = 0;
                    else
                        Active = Active + 1;
                }
                if (keyState.IsKeyDown(Keys.Up) && Elapsed > 0.3)
                {
                    Elapsed = 0;
                    if (Active == 0)
                        Active = Menu.Count() - 1;
                    else
                        Active = Active - 1;
                }
                #endregion
                #region Menülogik
                //foreach (GamePadState gamePadState in GamePads)
                //{
                    if (keyState.IsKeyDown(Keys.Enter) && Elapsed > 1)  //gamePadState.Buttons.Start == ButtonState.Pressed ||)
                    {
                        switch (Active)
                        {
                            case 0:
                                ScreenManager.LastScreen();
                                break;
                            case 1:
                                break;
                            case 2:
                                ScreenManager.Game.Exit();
                                break;
                            default:
                                break;
                        }
                   // }
                }
                #endregion

                base.Update(gameTime);
            }
        }

        #endregion

        #region Draw
        public override void Draw(GameTime gameTime)
        {
            if (IsActive == true)
            {
                ScreenManager.SpriteBatch.Begin();
                ScreenManager.SpriteBatch.Draw(Background, new Rectangle(0, 0, 800, 600), Color.White);
                ScreenManager.SpriteBatch.DrawString(Font, "Pause", new Vector2(300, 50), Color.White);
                #region Menü
                for (int i = 0; i < Menu.Count(); i++)
                {
                    if (i == Active)
                        ScreenManager.SpriteBatch.DrawString(Font, "" + Menu[i], new Vector2(200, 150 + i * 100), Color.Red);
                    else
                        ScreenManager.SpriteBatch.DrawString(Font, "" + Menu[i], new Vector2(200, 150 + i * 100), Color.White);
                }
                #endregion
                ScreenManager.SpriteBatch.End();
                base.Draw(gameTime);
            }

        }
        #endregion

    }
}
