﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;
#endregion

namespace SnakeUniverse
{
    public class Snake : TexturedDrawableGameComponent
    {
        public Snake(Game game,PlayScreen gamePlayScreen,int SnakeNumber,int x, int y,int parts, Vector2 border, String texHeadS, String texTailS, Color Col_Head)
            : base(game,gamePlayScreen)
        {
            for (int i = 0; i < parts; i++)
            {
                elementsX.Add(x + parts - i);
                elementsY.Add(y);
            }
            AreaBorder = border;
            this.Col_Head = Col_Head;
            this.SnakeNumber = SnakeNumber;
        }
        public Snake(Game game, PlayScreen gamePlayScreen,int SnakeNumber,int x,int y)
            : base(game, gamePlayScreen)
        {
            int parts = 4;
            for (int i = 0; i < parts; i++)
            {
                elementsX.Add(x + parts - i);
                elementsY.Add(y);
            }
            AreaBorder = new Vector2(800, 600);
            this.Col_Head = Color.Green;
            this.SnakeNumber = SnakeNumber;
        }

        // Ein Glied der Schlange wird auserhalb des Bildschirms erzeugt 
        public void AddElement()
        {
            elementsX.Add(-1);
            elementsY.Add(-1);
        }
        public bool IsDead
        {
            set { }
            get { return Death; }
        }
        // Idee
        //--------------------
        public bool HasTailInDirection
        {
            get { return hasTailInDirection ; }
        }
        bool hasTailInDirection;
        //-----------------------

        int snakeNumber;
        public int SnakeNumber
        {
            set { snakeNumber = value; }
            get {return snakeNumber; }
        }


        public void destroy() { Game.Components.Remove(this); }
        
        SpriteBatch spriteBatch;
        SpriteFont Font;
        bool Death = false;
        double Speed = 0.08f;
        Random random;
        
        Color Col_Tail = Color.White;
        Color Col_Head = Color.White;
        Texture2D Tex_Head, Tex_Tail;

        public ArrayList elementsX = new ArrayList();
        public ArrayList elementsY = new ArrayList();
        public float Elapsed;
        public Rectangle Head = new Rectangle(-1, -1, 10, 10);
        public Vector2 AreaBorder;
        bool DeathBorders = false;
        public String direction = "";
        bool canChangeDirection = true;
        int score;
        public int Score
        {         
            set { score = value; }
            get { return score; }
        }
        
        public Vector2 headPos { get { return new Vector2(Convert.ToSingle(elementsX[0]), Convert.ToSingle(elementsY[0])); } }

        public String Direction
        {
            set { if (canChangeDirection) { direction = value; canChangeDirection = false; } }
            get { return direction; }
        }

        public bool collision(Vector2 pos)
        {
            for (int i = 0; i < elementsX.Count; i++)
            {
                if (pos.X == Convert.ToSingle(elementsX[i]) && pos.Y == Convert.ToSingle(elementsY[i]))
                    return true;
            }
            return false;
        }

         protected override void LoadContent()   
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(this.Game.GraphicsDevice);
            Tex_Head= Game.Content.Load<Texture2D>("Textures/Snake_Head");;
            Tex_Tail = Game.Content.Load<Texture2D>("Textures/Snake_Tail");
            Font = Game.Content.Load<SpriteFont>("Fonts/Arial");

        }
        
        public override void Update(GameTime gameTime)
        {
            if (gamePlayScreen.IsActive == true && gamePlayScreen.gamestate == GameState.Running ) 
            {
                base.Update(gameTime);
                Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;    
            }          
            Head.X = Convert.ToInt16(elementsX[0])*10;
            Head.Y = Convert.ToInt16(elementsY[0])*10;

            #region Todesbedingung SelfKill
            for (int i = 1; i < elementsX.Count; i++)
            {
                if (elementsX[i].ToString() == elementsX[0].ToString() && elementsY[i].ToString() == elementsY[0].ToString())
                    Death = true;
                
            }
            #endregion

            #region Bildschirmrand
            if (AreaBorder.X > 0 || AreaBorder.Y > 0)
            {   
                
                if (Convert.ToInt16(elementsX[0]) < 0)//Links
                {
                    elementsX[0] = AreaBorder.X / 10-1;
                }
                if (Convert.ToInt16(elementsX[0]) >= (AreaBorder.X / 10)) //Rechts
                {
                    elementsX[0] = 0;
                }
                if (Convert.ToInt16(elementsY[0]) < 0) //Oben
                {
                    elementsY[0] = (AreaBorder.Y / 10)-1;
                }
                if (Convert.ToInt16(elementsY[0]) >= AreaBorder.Y / 10) //Unten
                {
                    elementsY[0] = 0;
                }
            }
            else if (DeathBorders)
            {
                if (Convert.ToInt16(elementsX[0]) < 0 &&
                    Convert.ToInt16(elementsX[0]) > (AreaBorder.X / 10) &&
                    Convert.ToInt16(elementsY[0]) < 0 &&
                    Convert.ToInt16(elementsY[0]) > AreaBorder.Y / 10)
                {
                    Death = true;
                }
            }
            #endregion

            #region Berechnung
            if (Death == false)
            {

                // Zeitlich gesteuerte geschwindigkeit
                if (Elapsed > Speed)
                {
                    Elapsed = 0;
                    if (direction == "Down")
                    {
                        elementsX.Insert(0, elementsX[0]);
                        elementsX.RemoveAt(elementsX.Count - 1);

                        elementsY.Insert(0, Convert.ToInt16(elementsY[0]) + 1);
                        elementsY.RemoveAt(elementsY.Count - 1);
                    }
                    if (direction == "Up")
                    {
                        elementsX.Insert(0, elementsX[0]);
                        elementsX.RemoveAt(elementsX.Count - 1);

                        elementsY.Insert(0, Convert.ToInt16(elementsY[0]) - 1);
                        elementsY.RemoveAt(elementsY.Count - 1);
                    }
                    if (direction == "Left")
                    {
                        elementsX.Insert(0, Convert.ToInt16(elementsX[0]) - 1);
                        elementsX.RemoveAt(elementsX.Count - 1);


                        elementsY.Insert(0, elementsY[0]);
                        elementsY.RemoveAt(elementsY.Count - 1);

                    }
                    if (direction == "Right")
                    {
                        elementsX.Insert(0, Convert.ToInt16(elementsX[0]) + 1);
                        elementsX.RemoveAt(elementsX.Count - 1);


                        elementsY.Insert(0, elementsY[0]);
                        elementsY.RemoveAt(elementsY.Count - 1);
                    }

                }
            #endregion
            }
            
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            
            spriteBatch.Begin();

            if (Tex_Head == null || Tex_Tail == null)
                return;
            
            for (int i = 0; i < elementsX.Count; i++)
            {
                spriteBatch.Draw(Tex_Tail, new Rectangle(Convert.ToInt16(elementsX[i]) * 10, Convert.ToInt16(elementsY[i]) * 10, 10, 10), Col_Tail);   
            }
            spriteBatch.Draw(Tex_Head, new Rectangle(Convert.ToInt16(elementsX[0]) * 10, Convert.ToInt16(elementsY[0]) * 10, 10, 10),Col_Head);        
            spriteBatch.DrawString(Font,"" + Score, new Vector2(headPos.X*10-5, headPos.Y*10-50), new Color(new Vector4(0.5f,0f, 0f,10f)));
            if (Death == true)
            {
                //spriteBatch.DrawString(Font, "Death", new Vector2(headPos.X*10, headPos.Y*10), new Color(new Vector4(0f,0f,0f,255f)));
                this.Col_Tail = Color.Gray;
            }
            else this.Col_Tail = Color.White;

            spriteBatch.End();
        }

        #region Sonstige Methoden
        public void IncreaseScore(int Value)
        {
            Score = Score + Value;
        }
        public void RandomPosition()
        {
            random = new Random();
            elementsX[0] = random.Next(1, 800);
            elementsY[0] = random.Next(1, 600);
        }
        public void Reset()
        {
            ResetScore();
            RemoveElements();
            Alive();
        }
        public void Die()
        {
            Death = true;
        }
        public void ResetScore()
        {
            Score = 0;
        }
        public void Alive()
        {
            Death = false;
        }
        public void RemoveElements()
        {
            elementsX.Clear();
            elementsY.Clear();
        }
        public void SetPosition(int X,int Y)
        {
            direction = "";
            elementsX.Add(X + 3);
            elementsY.Add(Y);
            elementsX.Add(X + 2);
            elementsY.Add(Y);
            elementsX.Add(X + 1);
            elementsY.Add(Y);
            elementsX.Add(X + 0);
            elementsY.Add(Y);
        }
        #endregion
    }
}