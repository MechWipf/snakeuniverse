﻿

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace SnakeUniverse
{
    public enum ScreenState
    {
        Active,
        Hidden,
    }
    public abstract class GameScreen
    {
        #region Initialisierung
        public bool otherScreenHasFocus = false;
        ScreenState screenState;
        ScreenManager screenManager;

        public ScreenState ScreenState
        {
            get { return screenState; }
            protected set { screenState = value; }
        }
        
        public ScreenManager ScreenManager
        {
            get { return screenManager; }
            internal set { screenManager = value; }
        }

        public bool IsActive
        {
            get
            {
                return(otherScreenHasFocus == false && screenState == ScreenState.Active) ;
            }
            //set { otherScreenHasFocus = value; }
        }
        #endregion


        public virtual void LoadContent() {}
        public virtual void UnloadContent() { }


        public virtual void Update(GameTime gameTime)
        {
            if (otherScreenHasFocus == true)
            {
               // screenState = ScreenState.Hidden;
            }
            else
            {
              //  screenState = ScreenState.Active;
            }
        }
        public virtual void Draw(GameTime gameTime) {
        }

        #region Sonstige Methoden
        public void ExitScreen()
        {
                ScreenManager.RemoveScreen(this);        
        }
        public void Inaktiv()
        {
            this.otherScreenHasFocus = true;
        }
        #endregion
    }
}
