﻿#region Using Statements

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Xml.Linq;


#endregion

namespace SnakeUniverse
{
    public class SplashScreen : GameScreen
    {


        #region Initializations
        Texture2D Splash;
        SpriteFont Font;
        public float Elapsed;
        #endregion

        #region LoadContent and UnloadContent
        public override void LoadContent()
        {
            Splash = ScreenManager.Game.Content.Load<Texture2D>("Textures/Backgrounds/Splash");
            Font = ScreenManager.Game.Content.Load<SpriteFont>("Fonts/Font");

            base.LoadContent();
        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }
        #endregion

        #region Update
        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {

                Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                KeyboardState keyState = new KeyboardState();
                keyState = Keyboard.GetState();
                if (keyState.IsKeyDown(Keys.Enter) && Elapsed > 1)
                {
                    Elapsed = 0;
                    ScreenManager.AddScreen(new TitleScreen());
                }
                    base.Update(gameTime);
            }
        }

        #endregion

        #region Draw
        public override void Draw(GameTime gameTime)
        {
            if (IsActive == true)
            {
                ScreenManager.SpriteBatch.Begin();
                    ScreenManager.SpriteBatch.Draw(Splash, ScreenManager.GraphicsDevice.Viewport.Bounds, null,
                    Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1);
                ScreenManager.SpriteBatch.End();
                
            }

        }
        #endregion

    }
}
