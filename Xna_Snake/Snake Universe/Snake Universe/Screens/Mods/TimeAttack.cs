﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;
#endregion

namespace SnakeUniverse
{
    /* Survival
     In Dem Modus "Survival" kommt es darauf an möglichst lang zu Überleben.
     Dabei ist egal wieviel Essen die Schlange aufnimmt.
     Wenn alle Spieler tot sein sollten gewinnt der Spieler mit den meisten Punkten.
     */
    class TimeAttack : PlayScreen
    {
        #region Deklaration
        int Foodmass = 5;
        int Goalcount = 3;
        int max = 0;
        int Victorynumber;
        float StartCount = 5;
        protected float Countdown;
        protected float Timer = 10;
        bool Message = false;
        #endregion
        public override void LoadContent()
        {
            base.LoadContent();
            Countdown = StartCount;
            #region Food erzeugen
            for (int i = 0; i < Foodmass; i++)
            {
                //Food food = new Food(ScreenManager.Game, this,Random.Next(0,800)/10,Random.Next(0,600)/10,"Snake_Food" );
                Food food = new Food(ScreenManager.Game, this);
                food.SetColor();
                ScreenManager.Game.Components.Add(food);
                Foods.Add(food);
            }
            #endregion
        }
        public override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            if (IsActive)
            {
                base.Update(gameTime);
                Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                #region Countdown
                for (int i = -1; i < Countdown; i++)
                {
                    if (Elapsed > 1)
                    {
                        Countdown = Countdown - 1;
                        Elapsed = 0;
                    }
                    else return;
                }
                if (Countdown < 0 && gamestate == GameState.Start)
                    gamestate = GameState.Running;
                #endregion
                if (gamestate == GameState.Running)
                {
                    #region Siegesbedingung
                        if (Elapsed > 1)
                        {
                            Timer = Timer - 1;
                            Elapsed = 0;
                        }
                        if (Timer < 0)
                        {
                            foreach (Snake snake in Snakes)
                            {
                                if (snake.Score > max)
                                {
                                    max = snake.Score;
                                    int Victorynumber = snake.SnakeNumber +1;   
                                }
                            //int max = SnakePoint.Max();
                            if (snake.Score == max)
                            {
		                       Console.WriteLine("Player: " + Victorynumber + " has won with Score: " + max);
                               gamestate = GameState.Victory;
                               continue;
                            }
                            }
                        }
                    }
                    #endregion
                
                if (gamestate == GameState.Victory)
                {
                    #region Any Key
                    if (Message == false)
                    {
                        Console.WriteLine("Press Any Key");
                        Message = true;
                    }
                    if (Keyboard.GetState().GetPressedKeys().Length > 0) //if (keyboardState.IsKeyDown(Keys.M))
                        gamestate = GameState.Over;
                    #endregion
                }
                if (gamestate == GameState.Over)
                {
                    #region Content Unload/ScreenRemove
                    if (ScreenManager.ScreenList.IndexOf(ScreenManager.currentScreen) + 1 < ScreenManager.ScreenList.Count())
                        ScreenManager.RemoveScreen(ScreenManager.ScreenList.ElementAt(ScreenManager.ScreenList.IndexOf(ScreenManager.currentScreen) + 1));
                    ScreenManager.RemoveScreen(this);
                    foreach (Snake snake in Snakes)
                        ScreenManager.Game.Components.Remove(snake);
                    foreach (Food foods in Foods)
                        ScreenManager.Game.Components.Remove(foods);
                    ScreenManager.FocusScreen(1);
                    #endregion
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            if (IsActive)
            {
                ScreenManager.SpriteBatch.Begin();
                if (Countdown >= 0)
                    ScreenManager.SpriteBatch.DrawString(BigFont, "" + Countdown, new Vector2(400, 100), Color.Black);
                else
                    if (Timer >= 0)
                    ScreenManager.SpriteBatch.DrawString(BigFont, "" + Timer, new Vector2(400, 100), Color.Black);

                ScreenManager.SpriteBatch.End();
            }
        }

    }
}
