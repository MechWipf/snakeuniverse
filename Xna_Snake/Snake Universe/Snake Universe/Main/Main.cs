#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;
#endregion

namespace SnakeUniverse
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {

#region Deklaration
        public GraphicsDeviceManager graphics;
        Vector2 scaleVector;
        ScreenManager screenManager;
#endregion
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            scaleVector = new Vector2(graphics.PreferredBackBufferWidth/800f,// / 1280f,
            graphics.PreferredBackBufferHeight/600f); // / 720f);

            Content.RootDirectory = "Content";
            screenManager = new ScreenManager(this, scaleVector);
        }

        protected override void Initialize()
        {
            screenManager.AddScreen(new SplashScreen());
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();   
            Components.Add(screenManager);             
            this.Window.Title = "Snake Universe by Finaldrace & Mechwipf";
            base.Initialize();
        }
        protected override void Update(GameTime gameTime)
        {
            screenManager.Update(gameTime);
             #region Steuerung
            KeyboardState ks = Keyboard.GetState();
             if (ks.IsKeyDown(Keys.Escape))
             {
                 this.Exit();
             }
             if (ks.IsKeyDown(Keys.F11))
             {
                 this.graphics.ToggleFullScreen();
                 this.graphics.ApplyChanges();
             }
             #endregion      
            base.Update(gameTime);
            }      
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
