﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;
#endregion

namespace SnakeUniverse
{
    public enum GameState
    {
        Victory,
        Running,
        Defeat,
        Over,
        Start
    }
    public class PlayScreen : GameScreen
    {     

        protected List<Snake> Snakes = new List<Snake>(4);
        protected List<Food> Foods = new List<Food>(10);
        bool Initialize = false;
        Texture2D Background_Game;
        Random Random = new Random();
        public float Elapsed;
        SoundEffect Njak;
        int PlayerNumber = 2;
        public Color[] colors = new Color[] {
                new Color(0f, 255f, 0f, 255f),
                new Color(0f, 0f, 255f, 255f),
                Color.Red,
                Color.HotPink
            };
        protected SpriteFont Font;
        protected SpriteFont BigFont;
        public GameState gamestate = GameState.Start ;
        public override void LoadContent()
        {
            base.LoadContent();

            if (Initialize == false)
            {
                #region Content
                Background_Game = ScreenManager.Game.Content.Load<Texture2D>("Textures/Backgrounds/Background_Game");
                Font = ScreenManager.Game.Content.Load<SpriteFont>("Fonts/Font");
                BigFont = ScreenManager.Game.Content.Load<SpriteFont>("Fonts/BigFont");
                Njak = ScreenManager.Game.Content.Load<SoundEffect>("Sounds/quaken");
                #endregion
                #region Snake erzeugen
                for (int i = 0; i < PlayerNumber; i++)
                {
                    //Snake snake = new Snake(ScreenManager.Game,this,i+1,5, 2 + (10 * i),4,new Vector2(800, 600), "Snake_Head", "Snake_Tail", colors[i]); 
                    Snake snake = new Snake(ScreenManager.Game, this, i+1,5,2+(10*i));
                    Snakes.Add(snake);
                    ScreenManager.Game.Components.Add(snake);
                }
                #endregion
                Initialize = true;
            }       
        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }
        private void Reset()
        {
            Console.WriteLine("Reset");
            int i = 0;
            foreach (Snake snake in Snakes)
            {
                snake.Reset();
                snake.SetPosition(5, 2 + (10 * i));//Die Methode muss ausgelagert werden(sprich nicht in Reset() ),sonst würden alle Schlangen an der gleichen Stelle erscheinen
                i++;
            }          
            foreach (Food food in Foods)
                food.Respawn();          
        }
        
        public override void Update(GameTime gameTime)
        {
            #region Controle
            KeyboardState keyboardState = Keyboard.GetState();
            ArrayList GamePads = new ArrayList();
            GamePads.Add(GamePad.GetState(PlayerIndex.One));
            GamePads.Add(GamePad.GetState(PlayerIndex.Two));
            GamePads.Add(GamePad.GetState(PlayerIndex.Three));
            GamePads.Add(GamePad.GetState(PlayerIndex.Four));
            #endregion
            if (IsActive & gamestate == GameState.Running)
            {
                #region Snake trifft Snake
                foreach (Snake snakeA in Snakes)
                {
                    foreach (Snake snakeB in Snakes)
                    {
                        if (snakeB.collision(snakeA.headPos))
                        {
                            if (snakeB == snakeA)
                                continue;
                            //snake.Die(); //Dann kann man noch score eintragen und so krams
                            snakeA.Die();
                            break;
                        }
                    }
                }
                #endregion
                #region Snake trifft Snake Intersect
                foreach (Snake snakeA in Snakes)
                {
                    foreach (Snake snakeB in Snakes)
                    {
                        if (snakeA.Head.Intersects(snakeB.Head))
                        {
                            if (snakeB == snakeA)
                            {
                                continue;
                            }
                            if (snakeA.IsDead == false & snakeB.IsDead == false)
                            {
                                //Njak.Play();    
                            }
                            //snakeA.Death = true;
                            //snakeB.Death = true;
                            break;
                        }
                    }
                }
                #endregion
                #region Snake trifft Food
                foreach (Snake snake in Snakes)
                {
                    //snake.Update(gameTime);   Erhöht die geschwindigkeit
                    foreach (Food food in Foods)
                    {
                        if (food.Pos.X == snake.headPos.X && food.Pos.Y == snake.headPos.Y)
                        {
                            snake.IncreaseScore(1);
                            food.Pos.X = Random.Next(10, Convert.ToInt16(800) / 10);
                            food.Pos.Y = Random.Next(10, Convert.ToInt16(600) / 10);
                            food.Col_Food = colors[Random.Next(0, 4)];
                            snake.AddElement();
                        }
                    }
                }
                #endregion
                #region Steuerung
                foreach (GamePadState gamePadState in GamePads)
                {
                    if (gamePadState.DPad.Left == ButtonState.Pressed || gamePadState.ThumbSticks.Left.X < 0 || keyboardState.IsKeyDown(Keys.A))
                    {
                        if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Right")//&& !Snakes[GamePads.IndexOf(gamePadState)].HasTailInDirection)
                            Snakes[GamePads.IndexOf(gamePadState)].direction = "Left";
                    }
                    if (gamePadState.DPad.Right == ButtonState.Pressed || gamePadState.ThumbSticks.Left.X > 0 || keyboardState.IsKeyDown(Keys.D))
                    {
                        if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Left")
                            Snakes[GamePads.IndexOf(gamePadState)].direction = "Right";
                    }
                    if (gamePadState.DPad.Up == ButtonState.Pressed || gamePadState.ThumbSticks.Left.Y > 0 || keyboardState.IsKeyDown(Keys.W))
                    {
                        if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Down")
                            Snakes[GamePads.IndexOf(gamePadState)].direction = "Up";
                    }
                    if (gamePadState.DPad.Down == ButtonState.Pressed || gamePadState.ThumbSticks.Left.Y < 0 || keyboardState.IsKeyDown(Keys.S))
                    {
                        if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Up")
                            Snakes[GamePads.IndexOf(gamePadState)].direction = "Down";
                    }

                    if (gamePadState.Triggers.Left == 1 && Elapsed > 0.09)
                    {
                        Elapsed = 0;
                        switch (Snakes[0].direction)
                        {
                            case "Up":
                                Snakes[0].direction = "Left";
                                break;
                            case "Left":
                                Snakes[0].direction = "Down";
                                break;
                            case "Down":
                                Snakes[0].direction = "Right";
                                break;
                            case "Right":
                                Snakes[0].direction = "Up";
                                break;
                        }
                    }
                    if (gamePadState.Triggers.Right == 1 && Elapsed > 0.07)
                    {
                        Elapsed = 0;
                        switch (Snakes[0].direction)
                        {
                            case "Up":
                                Snakes[0].direction = "Right";
                                break;
                            case "Left":
                                Snakes[0].direction = "Up";
                                break;
                            case "Down":
                                Snakes[0].direction = "Left";
                                break;
                            case "Right":
                                Snakes[0].direction = "Down";
                                break;
                        }
                    }
                    if (gamePadState.Buttons.Start == ButtonState.Pressed || keyboardState.IsKeyDown(Keys.Space) && Elapsed > 1.0)
                    {
                        Elapsed = 0;
                        if (ScreenManager.HasNextScreen)
                            ScreenManager.NextScreen();
                        else
                            ScreenManager.AddScreen(new PauseScreen());
                    }
                    if (keyboardState.IsKeyDown(Keys.R) && Elapsed > 1.0)
                    {
                        Elapsed = 0;
                        Reset();
                    }
                #endregion
            } 

                /* Die Abfrage if (IsActive && gamestate == GameState.Victory
                 * funktioniert nur bei unter Screens z.b Survival. Nicht beim PlayScreen direkt
                 */
            }                 
        }

        public override void Draw(GameTime gameTime)
        {
            if (IsActive)
            {
                ScreenManager.SpriteBatch.Begin();
                ScreenManager.SpriteBatch.Draw(Background_Game, new Rectangle(0, 0, 800, 600), Color.White);
                int z = 0;
                foreach (Food food in Foods)
                {
                    z = z + 20;
                    ScreenManager.SpriteBatch.DrawString(Font, "PosX" + food.Pos.X + "PosY" + food.Pos.Y, new Vector2(200, 400 + z), Color.Pink);
                }
                ScreenManager.SpriteBatch.End();
            }
        }
            
    }
}
