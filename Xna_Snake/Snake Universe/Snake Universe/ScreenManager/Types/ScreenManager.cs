#region Using Statements
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;

#endregion

namespace SnakeUniverse
{
    public class ScreenManager : DrawableGameComponent
    {
        #region Fields

        public List<GameScreen> ScreenList = new List<GameScreen>();
        List<GameScreen> screensToUpdate = new List<GameScreen>();
        ScaledSpriteBatch spriteBatch;
        SpriteFont font;
        bool otherScreenHasFocus = false;
        bool coveredByOtherScreen = false;
        public GameScreen currentScreen;
        Vector2 drawingScale;
        #endregion

        public bool HasNextScreen
        {
            get
            {
                if (ScreenList.IndexOf(currentScreen) + 1 < ScreenList.Count() ) //&& ScreenList.ElementAt(ScreenList.IndexOf(currentScreen)+1) == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        bool hasNextScreen;

        public ScaledSpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }
        public SpriteFont Font
        {
            get { return font; }
        }

        #region Initialization

        public ScreenManager(Game game,Vector2 drawingScale)
            : base(game)
        {
            this.drawingScale = drawingScale;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            ContentManager content = Game.Content;

            spriteBatch = new ScaledSpriteBatch(Game.GraphicsDevice, drawingScale);
            Game.Services.AddService(typeof(ScaledSpriteBatch), spriteBatch); 


            // Tell each of the screens to load their content.
            foreach (GameScreen screen in ScreenList)
            {
                screen.LoadContent();
            }
        }

        protected override void UnloadContent()
        {
            foreach (GameScreen screen in ScreenList)
            {
                screen.UnloadContent();
            }         
        }


        #endregion

        public override void Update(GameTime gameTime)
        {
            screensToUpdate.Clear();
            foreach (GameScreen screen in ScreenList)
                screensToUpdate.Add(screen);           

            while (screensToUpdate.Count > 0)
            {
                // Pop the topmost screen off the waiting list.
                GameScreen screen = screensToUpdate[screensToUpdate.Count - 1];
                screensToUpdate.RemoveAt(screensToUpdate.Count - 1);
                //if (screen.IsPopup)
                  //  IsCovered = true;
                // Update the screen.
                screen.Update(gameTime);
            }
        }


        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in ScreenList)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                    continue;
                screen.Draw(gameTime);
            }
        }


        #region Sonstige Methoden
        public void AddScreen(GameScreen screen)
        {
            if (currentScreen != null)
            {
                currentScreen.otherScreenHasFocus = true;
                Console.WriteLine("Old Screen: " + currentScreen + " Is Active :" + currentScreen.IsActive);
            }
            Console.WriteLine("New Screen: " + screen + " Is Active: " + screen.IsActive);
            screen.otherScreenHasFocus = false;
            ScreenList.Add(screen);
            screen.ScreenManager = this;
            currentScreen = screen;
            //currentScreen.otherScreenHasFocus = false;
            
            screen.LoadContent();
        }
        public void FocusScreen(int ScreenNumber)
        {
            currentScreen = ScreenList.ElementAt(ScreenNumber);
            currentScreen.otherScreenHasFocus = false;
            currentScreen.LoadContent();
            Console.WriteLine("New Screen/FocusScreen: " + currentScreen + " Is Active: " + currentScreen.IsActive);
            
        }
        public void RemoveScreen(GameScreen screen)
        {
            screen.UnloadContent();
            ScreenList.Remove(screen);
            screensToUpdate.Remove(screen);
        }
        // Enth�lt alle Screens
        public GameScreen[] GetScreens()
        {
            return ScreenList.ToArray();
        }

        public void LastScreen()
        {
            if (ScreenList.IndexOf(currentScreen) - 1 >= 0)
            {
                currentScreen.otherScreenHasFocus = true;
                Console.WriteLine("Old Screen: " + currentScreen + " Is Active :" + currentScreen.IsActive);
                currentScreen = ScreenList.ElementAt(ScreenList.IndexOf(currentScreen) - 1);
                currentScreen.otherScreenHasFocus = false;
                Console.WriteLine("Current/ LastScreen: " + currentScreen + " Is Active :" + currentScreen.IsActive);
                
              
            }
        }
        public void NextScreen()
        {
            if (ScreenList.IndexOf(currentScreen) + 1 < ScreenList.Count())
            {
                currentScreen.otherScreenHasFocus = true;
                Console.WriteLine("Old Screen: " + currentScreen + " Is Active :" + currentScreen.IsActive);
                currentScreen = ScreenList.ElementAt(ScreenList.IndexOf(currentScreen) + 1);
                currentScreen.otherScreenHasFocus = false;
                Console.WriteLine("Current/Next Screen: " + currentScreen + " Is Active :" + currentScreen.IsActive);
                
            }
        }
        #endregion
    }
}
