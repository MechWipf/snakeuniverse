﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;
#endregion

namespace SnakeUniverse
{
    public class Food : TexturedDrawableGameComponent
    {
        #region Deklaration
        public Food(Game game, PlayScreen gamePlayScreen, float x, float y, String TextureName)
            : base(game, gamePlayScreen)
        {
            Pos = new Vector2(x, y);
            this.TextureName = TextureName;
        }
        public Food(Game game, PlayScreen gamePlayScreen)
            : base(game, gamePlayScreen)
        {
            Pos = new Vector2(random.Next(1,800)/10,random.Next(1,600)/10);
            this.TextureName = "Snake_Food";
        }
        public Vector2 Pos;
        public Color Col_Food = Color.White;
        SpriteBatch spriteBatch;
        Texture2D Snake_Food;
        String TextureName = "Snake_Food";
        Random random = new Random();
        SpriteFont Font;
        public Color[] colors = new Color[] {
                new Color(0f, 255f, 0f, 255f),
                new Color(0f, 0f, 255f, 255f),
                Color.Red,
                Color.Yellow,
                Color.YellowGreen,
                Color.Turquoise,
                Color.Tomato,
                Color.SpringGreen,
                Color.SteelBlue
            };
        #endregion

        public void SetColor()
        {

            Col_Food = colors.ElementAt(random.Next(0,colors.Count() ) );
        }
        protected override void LoadContent()
        {
            base.LoadContent();
            
            spriteBatch = new SpriteBatch(this.Game.GraphicsDevice);
            Font = Game.Content.Load<SpriteFont>("Fonts/Font");
            Snake_Food = Game.Content.Load<Texture2D>("Textures/" + TextureName);
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            if (Snake_Food == null)
                return;

            spriteBatch.Begin();
            int i = 0;
            while (i < Pos.X)
            {
                spriteBatch.Draw(Snake_Food, new Rectangle(Convert.ToInt16(Pos.X) * 10, Convert.ToInt16(Pos.Y) * 10, 10, 10), Col_Food);
                i++;
            }
            spriteBatch.End();
        }

        #region Sonstige Methoden
        public void Respawn()
        {
            //random = new Random();
            Pos = new Vector2(random.Next(1, 800) / 10, random.Next(1, 600) / 10);
            //Convert.ToInt32(ScreenManager.Instance.dimensions.X/10));
            //Convert.ToInt32(ScreenManager.Instance.dimensions.Y/10));  
            SetColor();
        }
        #endregion
    }
}