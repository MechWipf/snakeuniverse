﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;

namespace SnakeUniverse
{
    public class Snake : DrawableGameComponent
    {
        public static Snake New(Game1 game, int x, int y, Vector2 border, String texHeadS, String texTailS,Color Col_Head)
        {
            Snake snake = new Snake(game);

            snake.elementsX.Add(x + 3);
            snake.elementsY.Add(y);
            snake.elementsX.Add(x + 2);
            snake.elementsY.Add(y);
            snake.elementsX.Add(x + 1);
            snake.elementsY.Add(y);
            snake.elementsX.Add(x + 0);
            snake.elementsY.Add(y);

            snake.texHeadS = texHeadS;
            snake.texTailS = texTailS;
            snake.Col_Head = Col_Head;
            snake.AreaBorder = border;
            
            return snake;
        }

        public void AddElement()
        {
            elementsX.Add(-1);
            elementsY.Add(-1);
        }
        public void destroy() { Game.Components.Remove(this); }


        SpriteBatch spriteBatch;
        SpriteFont Font;
        public bool Death = false;
        double Speed = 0.08f;
        
        Color Col_Tail = Color.White;
        Color Col_Head;
        String texHeadS, texTailS;
        Texture2D Tex_Head, Tex_Tail;

        public ArrayList elementsX = new ArrayList();
        public ArrayList elementsY = new ArrayList();
        public float Elapsed;

        public Snake(Game1 game) : base(game) { } // nicht klar
        public Vector2 AreaBorder;
        public bool DeathBorders = false;
        public String direction = "";
        bool canChangeDirection = true;
        public int Score = 0;
        public Vector2 headPos { get { return new Vector2(Convert.ToSingle(elementsX[0]), Convert.ToSingle(elementsY[0])); } }

        public String Direction
        {
            set { if (canChangeDirection) { direction = value; canChangeDirection = false; } }
            get { return direction; }
        }

        public bool collision(Vector2 pos)
        {
            for (int i = 0; i < elementsX.Count; i++)
            {
                if (pos.X == Convert.ToSingle(elementsX[i]) && pos.Y == Convert.ToSingle(elementsY[i]))
                    return true;
            }
            return false;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(this.Game.GraphicsDevice);

            Tex_Head = Game.Content.Load<Texture2D>(texHeadS);
            Tex_Tail = Game.Content.Load<Texture2D>(texTailS);
            Font = Game.Content.Load<SpriteFont>("Font");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            #region Todesbedingung
            for (int i = 1; i < elementsX.Count; i++)
            {
                if (elementsX[i].ToString() == elementsX[0].ToString() && elementsY[i].ToString() == elementsY[0].ToString())
                    Death = true;
                
            }
            #endregion

            #region Bildschirmrand
            if (AreaBorder.X > 0 || AreaBorder.Y > 0)
            {   
                
                if (Convert.ToInt16(elementsX[0]) < 0)//Links
                {
                    elementsX[0] = AreaBorder.X / 10-1;
                }
                if (Convert.ToInt16(elementsX[0]) >= (AreaBorder.X / 10)) //Rechts
                {
                    elementsX[0] = 0;
                }
                if (Convert.ToInt16(elementsY[0]) < 0) //Oben
                {
                    elementsY[0] = (AreaBorder.Y / 10)-1;
                }
                if (Convert.ToInt16(elementsY[0]) >= AreaBorder.Y / 10) //Unten
                {
                    elementsY[0] = 0;
                }
            }
            else if (DeathBorders)
            {
                if (Convert.ToInt16(elementsX[0]) < 0 &&
                    Convert.ToInt16(elementsX[0]) > (AreaBorder.X / 10) &&
                    Convert.ToInt16(elementsY[0]) < 0 &&
                    Convert.ToInt16(elementsY[0]) > AreaBorder.Y / 10)
                {
                    Death = true;
                }
            }
            #endregion

            #region Berechnung
            if (Death == false)
            {

                // Zeitlich gesteuerte geschwindigkeit
                if (Elapsed > Speed)
                {
                    Elapsed = 0;
                    if (direction == "Down")
                    {
                        elementsX.Insert(0, elementsX[0]);
                        elementsX.RemoveAt(elementsX.Count - 1);

                        elementsY.Insert(0, Convert.ToInt16(elementsY[0]) + 1);
                        elementsY.RemoveAt(elementsY.Count - 1);
                    }

                    if (direction == "Up")
                    {
                        elementsX.Insert(0, elementsX[0]);
                        elementsX.RemoveAt(elementsX.Count - 1);

                        elementsY.Insert(0, Convert.ToInt16(elementsY[0]) - 1);
                        elementsY.RemoveAt(elementsY.Count - 1);
                    }

                    if (direction == "Left")
                    {
                        elementsX.Insert(0, Convert.ToInt16(elementsX[0]) - 1);
                        elementsX.RemoveAt(elementsX.Count - 1);


                        elementsY.Insert(0, elementsY[0]);
                        elementsY.RemoveAt(elementsY.Count - 1);

                    }

                    if (direction == "Right")
                    {
                        elementsX.Insert(0, Convert.ToInt16(elementsX[0]) + 1);
                        elementsX.RemoveAt(elementsX.Count - 1);


                        elementsY.Insert(0, elementsY[0]);
                        elementsY.RemoveAt(elementsY.Count - 1);
                    }

                }
            #endregion
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            spriteBatch.Begin();

            if (Tex_Head == null || Tex_Tail == null)
                return;
            for (int i = 0; i < elementsX.Count; i++)
            {
                spriteBatch.Draw(Tex_Tail, new Rectangle(Convert.ToInt16(elementsX[i]) * 10, Convert.ToInt16(elementsY[i]) * 10, 10, 10), Col_Tail);   
            }
            spriteBatch.Draw(Tex_Head, new Rectangle(Convert.ToInt16(elementsX[0]) * 10, Convert.ToInt16(elementsY[0]) * 10, 10, 10),Col_Head);        
            spriteBatch.DrawString(Font,"" + Score, new Vector2(headPos.X*10-5, headPos.Y*10-50), new Color(new Vector4(0.5f,0f, 0f,10f)));
            if (Death == true)
            {
                //spriteBatch.DrawString(Font, "Death", new Vector2(headPos.X*10, headPos.Y*10), new Color(new Vector4(0f,0f,0f,255f)));
                this.Col_Tail = Color.Gray;
            }

            spriteBatch.End();
        }

    }
}