﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;

namespace SnakeUniverse
{
    class Food : DrawableGameComponent
    {
        public static Food New(Game1 game, float x, float y, String texFoodS,Color Col_Food)
        {
            Food food = new Food(game);

            food.Pos = new Vector2(x, y);
            food.texFoodS = texFoodS;
            food.Col_Food = Col_Food;

            return food;
        }
        


        public Food(Game1 game) : base(game) { }

        public Vector2 Pos;

        public Color Col_Food = Color.White;
        SpriteBatch spriteBatch;
        Texture2D Snake_Food;
        String texFoodS;

        #region loadContent
        protected override void LoadContent()
        {
            base.LoadContent();

            spriteBatch = new SpriteBatch(this.Game.GraphicsDevice);
            Snake_Food = Game.Content.Load<Texture2D>(texFoodS);
        }
        #endregion

        #region draw
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            if (Snake_Food == null)
                return;

            spriteBatch.Begin();
            int i = 0;
            while (i < Pos.X)
            {
                spriteBatch.Draw(Snake_Food, new Rectangle(Convert.ToInt16(Pos.X) * 10, Convert.ToInt16(Pos.Y) * 10, 10, 10), Col_Food);
                i++;
            }
            spriteBatch.End();
        }
        #endregion
    }
}