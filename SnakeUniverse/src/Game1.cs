using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;

namespace SnakeUniverse
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {

#region Deklaration
        public GraphicsDeviceManager graphics;
        Texture2D Background, Background_Game;
        public float Elapsed ;
        SpriteBatch spriteBatch;
        public Random Rand = new Random();

        public Color[] colors = new Color[] {
                new Color(0f, 255f, 0f, 255f),
                new Color(0f, 0f, 255f, 255f),
                Color.Red,
                Color.HotPink
            };

        List<Snake> Snakes = new List<Snake>(4);
        List<Food> Foods = new List<Food>(10);
#endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            ScreenManager.Instance.Initialize();
            ScreenManager.Instance.Dimensions = new Vector2(800, 600);
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();
            this.Window.Title = "Snake Universe by Finaldrace & Mechwipf";
            #region Snake erzeugen
            for (int i = 0; i < 4; i++)
            {
                //Snakes.Add(Snake.New(this, 5, 2 + (10 * i), new Vector2(800, 600), "Snake_Head", "Snake_Tail", colors[i]));
            }
            
            foreach (Snake snake in Snakes)
            {
             this.Components.Add(snake);  // Verwaltet unsere Objekte (ruft Draw, Update usw auf)   
            }
            #endregion
            #region Food erzeugen
            for (int i = 0; i < 4; i++)
            {
              Foods.Add(Food.New(
                  this,
                  Rand.Next(1, graphics.PreferredBackBufferWidth/10),
                  Rand.Next(1, graphics.PreferredBackBufferHeight/10),
                  "Snake_Food",
                  colors[Rand.Next(0,4)]));
           
            }
            foreach (Food food in Foods)
            {
                this.Components.Add(food);
            }
            base.Initialize();
            #endregion
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Background = Content.Load<Texture2D>("Background");
            Background_Game = Content.Load<Texture2D>("Background_Game");
            ScreenManager.Instance.LoadContent(Content);
            
   
        }

        protected override void UnloadContent()
        {

        }

        private void Reset()
        {
            foreach (Snake snake in Snakes)
            {
                this.Components.Remove(snake);  // Verwaltet unsere Objekte (ruft Draw, Update usw auf)   
            }
            for (int i = 0; i < Snakes.Count-1; i++)
            {
                //Snakes[i] = Snake.New(ref this, 5, 2 + (10 * i), new Vector2(800, 600), "Snake_Head", "Snake_Tail", colors[i]);
            }
            foreach (Snake snake in Snakes)
            {
                this.Components.Add(snake);  // Verwaltet unsere Objekte (ruft Draw, Update usw auf)   
            }
        }

        protected override void Update(GameTime gameTime)
        {
            ScreenManager.Instance.Update(gameTime);
            KeyboardState ks = Keyboard.GetState();
             Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;

           
                if (ks.IsKeyDown(Keys.B) && Elapsed > 0.5)
                {
                    Elapsed = 0;
                    ScreenManager.Instance.LastScreen();
                }
                if (ks.IsKeyDown(Keys.N) && Elapsed > 0.5)
                {
                    Elapsed = 0;
                    ScreenManager.Instance.NextScreen();
                }

            base.Update(gameTime);
            }
        
#region Draw
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            
            //spriteBatch.Draw(Background_Game, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
            ScreenManager.Instance.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
#endregion
    }
}
