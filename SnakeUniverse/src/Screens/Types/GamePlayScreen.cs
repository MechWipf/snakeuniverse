﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;


namespace Snake_Universe
{
    public class GamePlayScreen : GameScreen
    {
        
        Vector2 Pos;

        List<Snake> Snakes = new List<Snake>(4);
        List<Food> Foods = new List<Food>(10);
        bool Initialize = false;
        Texture2D Snake_Head;
        KeyboardState keyState;
        private Game1 game;
        Random Random = new Random();
        public float Elapsed;
        
        public Color[] colors = new Color[] {
                new Color(0f, 255f, 0f, 255f),
                new Color(0f, 0f, 255f, 255f),
                Color.Red,
                Color.HotPink
            };
        
        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            Snake_Head = this.content.Load<Texture2D>("Snake_Head");

            if (Initialize == false)
            {
                Pos.X = 100;
                Pos.Y = 100;
                #region Snake erzeugen
                for (int i = 0; i < 4; i++)
                {
                    //Snakes.Add(Snake.New(game, 5, 2 + (10 * i), new Vector2(800, 600), "Snake_Head", "Snake_Tail", colors[i]));
                }

                foreach (Snake snake in Snakes)
                {
                    //game.Components.Add(snake);  // Verwaltet unsere Objekte (ruft Draw, Update usw auf)   
                    //snake.LoadContent();
                }
                #endregion
                #region Food erzeugen
                for (int i = 0; i < 4; i++)
                {
                    Foods.Add(Food.New(
                        game,
                        Random.Next(1, Convert.ToInt16(ScreenManager.Instance.dimensions.X) / 10),
                        Random.Next(1, Convert.ToInt16(ScreenManager.Instance.dimensions.Y) / 10),
                        "Snake_Food",
                        colors[Random.Next(0, 4)]));

                }
                foreach (Food food in Foods)
                {
                    //game.Components.Add(food);
                }
                #endregion
                Initialize = true;
            }
           
            
        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        private void Reset()
        {
            foreach (Snake snake in Snakes)
            {
                //game.Components.Remove(snake);  // Verwaltet unsere Objekte (ruft Draw, Update usw auf)   
            }
            for (int i = 0; i < Snakes.Count - 1; i++)
            {
                Snakes[i] = Snake.New(game, 5, 2 + (10 * i), new Vector2(800, 600), "Snake_Head", "Snake_Tail", colors[i]);
            }
            foreach (Snake snake in Snakes)
            {
               // game.Components.Add(snake);  // Verwaltet unsere Objekte (ruft Draw, Update usw auf)   
            }
        }

        public override void Update(GameTime gameTime)
        {

            #region Snake trifft Snake
            foreach (Snake snakeA in Snakes)
            {
                foreach (Snake snakeB in Snakes)
                {
                    if (snakeB.collision(snakeA.headPos))
                    {
                        if (snakeB == snakeA)
                        {
                            continue;
                        }
                        //snake.Die(); //Dann kann man noch score eintragen und so krams
                        snakeA.Death = true;
                        break;
                    }
                }
            }
            #endregion
            #region Steuerung
            KeyboardState ks = Keyboard.GetState();

            Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            ArrayList GamePads = new ArrayList();

            GamePads.Add(GamePad.GetState(PlayerIndex.One));
            GamePads.Add(GamePad.GetState(PlayerIndex.Two));
            GamePads.Add(GamePad.GetState(PlayerIndex.Three));
            GamePads.Add(GamePad.GetState(PlayerIndex.Four));

            foreach (GamePadState gamePadState in GamePads)
            {
                if (gamePadState.DPad.Left == ButtonState.Pressed || gamePadState.ThumbSticks.Left.X < 0 || ks.IsKeyDown(Keys.A))
                {
                    if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Right")
                        Snakes[GamePads.IndexOf(gamePadState)].direction = "Left";
                }
                if (gamePadState.DPad.Right == ButtonState.Pressed || gamePadState.ThumbSticks.Left.X > 0 || ks.IsKeyDown(Keys.D))
                {
                    if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Left")
                        Snakes[GamePads.IndexOf(gamePadState)].direction = "Right";
                }
                if (gamePadState.DPad.Up == ButtonState.Pressed || gamePadState.ThumbSticks.Left.Y > 0 || ks.IsKeyDown(Keys.W))
                {
                    if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Down")
                        Snakes[GamePads.IndexOf(gamePadState)].direction = "Up";
                }
                if (gamePadState.DPad.Down == ButtonState.Pressed || gamePadState.ThumbSticks.Left.Y < 0 || ks.IsKeyDown(Keys.S))
                {
                    if (Snakes[GamePads.IndexOf(gamePadState)].direction != "Up")
                        Snakes[GamePads.IndexOf(gamePadState)].direction = "Down";
                }

                if (gamePadState.Triggers.Left == 1 && Elapsed > 0.09)
                {
                    Elapsed = 0;
                    switch (Snakes[0].direction)
                    {
                        case "Up":
                            Snakes[0].direction = "Left";
                            break;
                        case "Left":
                            Snakes[0].direction = "Down";
                            break;
                        case "Down":
                            Snakes[0].direction = "Right";
                            break;
                        case "Right":
                            Snakes[0].direction = "Up";
                            break;
                    }
                }
                if (gamePadState.Triggers.Right == 1 && Elapsed > 0.07)
                {
                    Elapsed = 0;
                    switch (Snakes[0].direction)
                    {
                        case "Up":
                            Snakes[0].direction = "Right";
                            break;
                        case "Left":
                            Snakes[0].direction = "Up";
                            break;
                        case "Down":
                            Snakes[0].direction = "Left";
                            break;
                        case "Right":
                            Snakes[0].direction = "Down";
                            break;
                    }
                }

                if (gamePadState.Buttons.Start == ButtonState.Pressed || ks.IsKeyDown(Keys.Enter))
                {
                    Reset();
                }

            }

            if (ks.IsKeyDown(Keys.Escape))
            {
                game.Exit();
            }

            if (ks.IsKeyDown(Keys.F11))
            {
                game.graphics.ToggleFullScreen();
                game.graphics.ApplyChanges();
            }
            #endregion
            #region Snake trifft Food
            foreach (Snake snake in Snakes)
            {
                //snake.Update(gameTime);   Erhöht die geschwindigkeit
                foreach (Food food in Foods)
                {
                    if (food.Pos.X == snake.headPos.X && food.Pos.Y == snake.headPos.Y)
                    {
                        snake.Score = snake.Score + 1;
                        food.Pos.X = Random.Next(10, Convert.ToInt16(ScreenManager.Instance.dimensions.X) / 10);
                        food.Pos.Y = Random.Next(10, Convert.ToInt16(ScreenManager.Instance.dimensions.Y) / 10);
                        food.Col_Food = colors[Random.Next(0, 4)];
                        snake.AddElement();
                    }
                }
            }
            #endregion       

            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.D3))
            {
                ScreenManager.Instance.AddScreen(new TitleScreen());
            }
            if (keyState.IsKeyDown(Keys.D1))
            {
                ScreenManager.Instance.AddScreen(new SplashScreen());
            }

            if (keyState.IsKeyDown(Keys.W))
            {
                Pos.Y--;
            }
            if (keyState.IsKeyDown(Keys.D))
            {
                Pos.X++;
            }
            if (keyState.IsKeyDown(Keys.S))
            {
                Pos.Y++; 
            }
            if (keyState.IsKeyDown(Keys.A))
            {
                Pos.X--;
            }
            // Statt Components.Add
            foreach (Snake snakes in Snakes)
            {
                snakes.Update(gameTime);
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            // Statt Components.Add
            foreach (Snake snakes in Snakes)
            {
                snakes.Draw(gameTime);
            }
            spriteBatch.Draw(Snake_Head, new Rectangle(Convert.ToInt16(Pos.X), Convert.ToInt16(Pos.Y), 100, 100), Color.White);
        }
            
        public GameTime gameTime { get; set; }
    }
}
