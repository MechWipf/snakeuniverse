﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SnakeUniverse
{
    public class SplashScreen : GameScreen
    {
        KeyboardState keyState;
         SpriteFont Font;
         Texture2D Splash;

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            //if (Font == null)
                Font = this.content.Load<SpriteFont>("Font");
                Splash = this.content.Load<Texture2D>("Splash");

        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.D3))
            {
                ScreenManager.Instance.AddScreen(new TitleScreen());
            }
            if (keyState.IsKeyDown(Keys.D2))
            {
                ScreenManager.Instance.AddScreen(new GamePlayScreen());
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.DrawString(Font, "SplashScreen", new Vector2(100, 100), Color.Black);
            spriteBatch.Draw(Splash,new Rectangle(0, 0, 800,600),Color.White);    
        }
    }
}
