﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SnakeUniverse
{
    public class TitleScreen : GameScreen
    {
        KeyboardState keyState;
        SpriteFont Font;
        List<string>Menu = new List<string>();
        int Active = 0;
        public float Elapsed;
        bool Initialize = false;

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            //if (Font == null)
            Font = this.content.Load<SpriteFont>("Font");
            if (Initialize == false)
            {
                Menu.Add("Neues Spiel");
                Menu.Add("Splashscreen");
                Menu.Add("Ende");
            }
            Initialize = true;

        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Down) && Elapsed > 0.3)
            {
                Elapsed = 0;
                if (Active == Menu.Count()-1)
                {
                    Active = 0;
                }
                else
                {
                    Active = Active + 1;    
                }               
            }
            if (keyState.IsKeyDown(Keys.Up) && Elapsed > 0.3)
            {
                Elapsed = 0;
                if (Active == 0)
                {
                    Active = Menu.Count()-1;
                }
                else
                {
                    Active = Active - 1;
                }  
            }

            if (keyState.IsKeyDown(Keys.D1))
            {
                ScreenManager.Instance.AddScreen(new SplashScreen());
            }
            if (keyState.IsKeyDown(Keys.D2))
            {
                ScreenManager.Instance.AddScreen(new GamePlayScreen());
            }
            if (keyState.IsKeyDown(Keys.Enter))
            {
                switch (Active)
                {
                    case 0:
                        ScreenManager.Instance.AddScreen(new GamePlayScreen());
                        break;
                    case 1:
                        ScreenManager.Instance.AddScreen(new SplashScreen());
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Menu.Count(); i++)
            {
                if (i == Active)
                {
                    spriteBatch.DrawString(Font, ""+ Menu[i], new Vector2(100, 100+i*50), Color.Red);  
                }
                else
	            {
                   spriteBatch.DrawString(Font, ""+ Menu[i], new Vector2(100, 100+i*50), Color.Black);    
	            }               
            }
            spriteBatch.DrawString(Font, "Aktive" + Active, new Vector2(330, 100), Color.Beige);
            spriteBatch.DrawString(Font, "Anzahl" + Menu.Count(), new Vector2(200, 100), Color.Beige);
        }
    }
}
