﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SnakeUniverse
{
    public class GamePlayScreen : GameScreen
    {
        KeyboardState keyState;

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);

        }
        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Z))
            {
                ScreenManager.Instance.AddScreen(new TitleScreen());
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.DrawString(Font, "SplashScreen", new Vector2(100, 100), Color.Black);
            //spriteBatch.Draw(Splash, new Rectangle(0, 0, 800, 600), Color.White);
        }
    }
}
