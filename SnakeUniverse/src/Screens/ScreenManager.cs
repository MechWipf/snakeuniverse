﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SnakeUniverse
{
    public class ScreenManager
    {
        #region Variablen

        public GameScreen currentScreen;
        GameScreen newScreen;
        SpriteFont Font;
        /// <summary>
        /// Creating custom Content Manager
        /// </summary>
        ContentManager content;

        private static ScreenManager instance;
        List<GameScreen> screenList = new List<GameScreen>();

        public Vector2 dimensions;


        #endregion

        #region Eigenschaften

        public static ScreenManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new ScreenManager();
                return instance;
            }
        }

        public ContentManager Content
        {
            get { return content; }
        }


        public Vector2 Dimensions
        {
            get { return dimensions; }
            set { dimensions = value; }
        }

        #endregion

        #region Haupt Methoden

        public void AddScreen(GameScreen screen)
        {
            screenList.Add(screen);
            currentScreen.UnloadContent();
            currentScreen = screen;
            currentScreen.LoadContent(content);
        }
        public void DeleteScreen(GameScreen screen)
        {
            if (screen != currentScreen)
            {
                screenList.Remove(screen);
                screen.UnloadContent();   
            }
        }
        public void LastScreen()
        {
            if (screenList.IndexOf(currentScreen)-1 > -1 )
            {
                currentScreen = screenList.ElementAt(screenList.IndexOf(currentScreen) - 1);
                currentScreen.LoadContent(content);   
            }
        }
        public void NextScreen()
        {
            if (screenList.IndexOf(currentScreen)+1  < screenList.Count())// 
            {
                currentScreen = screenList.ElementAt(screenList.IndexOf(currentScreen) + 1);
                currentScreen.LoadContent(content);
            }
        }

        public void Initialize()
        {
            currentScreen = new SplashScreen();        
            screenList.Add(currentScreen);
        }

        public void LoadContent(ContentManager Content)
        {
            content = new ContentManager(Content.ServiceProvider,"Content");           
            currentScreen.LoadContent(content);
            Font = this.content.Load<SpriteFont>("Font");
        }
        public void Update(GameTime gameTime)
        {
                currentScreen.Update(gameTime);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            currentScreen.Draw(spriteBatch);
            spriteBatch.DrawString(Font, "Screens:" + screenList.Count(), new Vector2(10, 100), new Color(new Vector4(200f, 200f, 200f, 10f)));
            spriteBatch.DrawString(Font, "Current:" + screenList.IndexOf(currentScreen), new Vector2(10, 200), new Color(new Vector4(200f, 200f, 200f, 10f)));  
            
            
        }

        #endregion

    }
}
